﻿namespace Birthdayinator
{
    partial class DateWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_pickadate = new System.Windows.Forms.Label();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.lbl_whose = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_pickadate
            // 
            this.lbl_pickadate.AutoSize = true;
            this.lbl_pickadate.Location = new System.Drawing.Point(12, 28);
            this.lbl_pickadate.Name = "lbl_pickadate";
            this.lbl_pickadate.Size = new System.Drawing.Size(61, 13);
            this.lbl_pickadate.TabIndex = 1;
            this.lbl_pickadate.Text = "Pick a date";
            // 
            // calendar
            // 
            this.calendar.Location = new System.Drawing.Point(4, 50);
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 2;
            this.calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.DateSelected);
            // 
            // lbl_whose
            // 
            this.lbl_whose.AutoSize = true;
            this.lbl_whose.Location = new System.Drawing.Point(1, 231);
            this.lbl_whose.Name = "lbl_whose";
            this.lbl_whose.Size = new System.Drawing.Size(157, 13);
            this.lbl_whose.TabIndex = 3;
            this.lbl_whose.Text = "No one`s birthday is on this day.";
            // 
            // DateWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lbl_whose);
            this.Controls.Add(this.calendar);
            this.Controls.Add(this.lbl_pickadate);
            this.Name = "DateWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_pickadate;
        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.Label lbl_whose;
    }
}

