﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Birthdayinator
{
    public partial class DateWindow : Form
    {
        private Dictionary<DateTime, string> dates = new Dictionary<DateTime, string>();

        public DateWindow(string user)
        {
            InitializeComponent();
            InitDates(user);
        }

        private void DateSelected(object sender, DateRangeEventArgs e)
        {
            string name;

            if(dates.TryGetValue(calendar.SelectionStart.Date, out name))
            {
                lbl_whose.Text = name + "`s birthday is on this date!";
            }

            else
            {
                lbl_whose.Text = name + "No one`s birthday is on this date.";
            }
            
        }

        private void InitDates(string user)
        {
            string line;
            System.IO.StreamReader file;
            string dir = user + "BD.txt";

            try
            {
                file = new System.IO.StreamReader(@dir);
                file.Close();
      
            }

            catch (Exception e)
            {
                   MessageBox.Show("Error: users file not found. Please restart the program.");
                System.IO.StreamWriter createdFile = new System.IO.StreamWriter(@dir);
                createdFile.Close();
            }


            file = new System.IO.StreamReader(@dir);

            while((line = file.ReadLine()) != null)
            {
                string[] parsedLine = line.Split(',');
                dates.Add(ParseDate(parsedLine[1]), parsedLine[0]);
            }

            file.Close();



        }

        private DateTime ParseDate(string date)
        {
       
            string[] dateParsed = date.Split('/');
            int day = int.Parse(dateParsed[1]);
            int month = int.Parse(dateParsed[0]);
            int year =  int.Parse(dateParsed[2]);

            return new DateTime(year, month, day);

        }
    }
}
