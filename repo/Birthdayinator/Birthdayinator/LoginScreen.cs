﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Birthdayinator
{
    public partial class LoginScreen : Form
    {
        private Dictionary<string, string> users = new Dictionary<string, string>();

        public LoginScreen()
        {
            InitializeComponent();
            InitUsers();
        }

        private void InitUsers()
        {
            string line;
            System.IO.StreamReader file;

            try
            {
                file = new System.IO.StreamReader(@"users.txt");
                while ((line = file.ReadLine()) != null)
                {
                    string[] creds = line.Split(',');
                    users.Add(creds[0], creds[1]);
                }

                file.Close();
            }

            catch(Exception e)
            {
                MessageBox.Show("Error: users file not found. Please restart the program.");
            }
            

        
        }

        private void ExitProgram(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login(object sender, EventArgs e)
        {
            string username = txtb_uname.Text;
            string password;

            if(users.TryGetValue(username, out password))
            {
                if(password == txtb_pass.Text)
                {
                    MessageBox.Show("Logged in successfully. Welcome, " + username + "!");
                    this.Hide();
                    Form newWindow = new DateWindow(username);
                    newWindow.ShowDialog();
                    this.Close();
                }

                else
                {
                    MessageBox.Show("Whoops! Wrong credentials!");
                }
            }

            else
            {
                MessageBox.Show("Whoops! Wrong credentials!");
            }
        }
    }
}
